﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Senacao.Views
{
    public class Servico
    {
        public string Nome { get; set; }
        public float Preco { get; set; }
        public float PrecoOpcionais { get; set; }
        public string PrecoFormatado
        {
            get { return string.Format("R$ {0}", Preco);  } 
        }
    }

    public partial class ListagemView : ContentPage
    {
        public List<Servico> Servicos { get; set;  } 

        public ListagemView()
        {
            InitializeComponent();

            this.Servicos = new List<Servico>
            {
                new Servico { Nome = "Banho e Tosa", Preco = 80 },
                new Servico { Nome = "Tosa Higiênica", Preco = 60 },
                new Servico { Nome = "Consulta Veterinária", Preco = 150},
                new Servico { Nome = "Retorno Consulta", Preco = 50 },
                new Servico { Nome = "Exames", Preco = 120 }
            };

            this.BindingContext = this;
        }

        private void ListViewServicos_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var servico = (Servico)e.Item;

            Navigation.PushAsync( new DescricaoView(servico) );

            //DisplayAlert("Serviço", string.Format("" +
            //    "Você selecionou o serviço '{0}'. Valor {1}",
            //    servico.Nome, servico.PrecoFormatado), "OK");

        }

        private void ButtonAgendamentos_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ListagemAgendamentosView());
        }

  
    }
}
