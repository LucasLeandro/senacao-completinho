﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Senacao.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DescricaoView : ContentPage
	{
        private const float TRANSPORTE = 80;
        private const float BANHO_RAPIDO = 40;
        private const float PASSEIO = 30;
        private const float MASSAGEM = 25;
   
        public string TextoTransporte
        {
            get
            {
                return string
                    .Format("Transporte - R$ {0}", TRANSPORTE);
            }
        }

        public string TextoBanhoRapido
        {
            get
            {
                return string
                    .Format("Banho Rápido - R$ {0}", BANHO_RAPIDO);
            }
        }
        public string TextoPasseio
        {
            get
            {
                return string
                    .Format("Passeio - R$ {0}", PASSEIO);
            }
        }

        public string TextoMassagem
        {
            get
            {
                return string
                    .Format("Massagem - R$ {0}", MASSAGEM);
            }
        }

        public string ValorTotal
        {
            get
            {
                float precoOpcionais = 
                (IncluiTransporte ? TRANSPORTE : 0)
                + (IncluiBanhoRapido ? BANHO_RAPIDO : 0)
                + (IncluiPasseio ? PASSEIO : 0)
                + (IncluiMassagem ? MASSAGEM : 0);

                Servico.PrecoOpcionais = precoOpcionais;

                return string.Format("Valor total: R$ {0}",
                    Servico.Preco + precoOpcionais);
                    
            }
        }

        // Capturando opcionais --

        bool incluiTransporte;
        public bool IncluiTransporte
        {
            get
            {
                return incluiTransporte;
            }

            set
            {
                incluiTransporte = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiTransporte)
                //    DisplayAlert("Transporte", "Ativo", "OK");
                //else
                //    DisplayAlert("Transporte", "Inativo", "OK");
            }
        }

        bool incluiBanhoRapido;
        public bool IncluiBanhoRapido
        {
            get
            {
                return incluiBanhoRapido;
            }

            set
            {
                incluiBanhoRapido = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiBanhoRapido)
                //    DisplayAlert("Transporte", "Ativo", "OK");
                //else
                //    DisplayAlert("Transporte", "Inativo", "OK");
            }
        }

        bool incluiPasseio;
        public bool IncluiPasseio
        {
            get
            {
                return incluiPasseio;
            }

            set
            {
                incluiPasseio = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiPasseio)
                //    DisplayAlert("Transporte", "Ativo", "OK");
                //else
                //    DisplayAlert("Transporte", "Inativo", "OK");
            }
        }

        bool incluiMassagem;
        public bool IncluiMassagem
        {
            get
            {
                return incluiMassagem;
            }

            set
            {
                incluiMassagem = value;
                OnPropertyChanged(nameof(ValorTotal));
                //if (incluiMassagem)
                //    DisplayAlert("Transporte", "Ativo", "OK");
                //else
                //    DisplayAlert("Transporte", "Inativo", "OK");
            }
        }

        public Servico Servico { get; set; }
		public DescricaoView (Servico servico)
		{
			InitializeComponent();

            this.Title = servico.Nome;
            this.Servico = servico;
            this.BindingContext = this;
            
		}

        private void ButtonProximo_Clicked(object sender, EventArgs e)
        {
         
            Navigation.PushAsync(new AgendamentoView(this.Servico));
        }
    }
}